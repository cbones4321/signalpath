# README #

1. Rank each of the following languages and frameworks on a 4 point scale, where 1 = "I've been paid to write production quality code with it." and 4 = "I could write a Hello World web application if I had a tutorial."
* JQuery - 4
* Python - 3
* PHP - 1
* Scala - 2
* Clojure - 2
* Haskell - 1
* Golang - 2
* Node.js - 4
* Ruby - 2
* Java - 4
* Angular - 3
* Chef - 1

2. Please find the convert utility in the project `hex-convert-base64`. It is a spring boot application.

3. Please find the scala project in the directory `convert-python-script`. 