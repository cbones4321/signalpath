package com.calvinmmiller.signalpath.hexconvertbase64.utility;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class ConverterTests {

    @Test
    public void test_hex_to_base64_success() throws Exception{
        assertEquals("RXZpZGludA==", Converter.convertHexToBase64("45766964696e74"));
    }

    @Test
    public void test_hex_to_base64_null_throws_npe() throws Exception{
        Exception e = assertThrows(NullPointerException.class, () -> Converter.convertHexToBase64(null));
        assertEquals("Please enter a hexadecimal to convert to base64", e.getMessage());
    }

    @Test
    public void test_hex_to_base64_not_hex_fail() throws Exception {
        assertEquals("Please pass a standard hexadecimal number", Converter.convertHexToBase64("This wont work!"));
    }
}
