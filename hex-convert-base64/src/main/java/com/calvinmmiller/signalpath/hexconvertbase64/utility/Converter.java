package com.calvinmmiller.signalpath.hexconvertbase64.utility;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class Converter {

    public static String convertHexToBase64(String s){
        Objects.requireNonNull(s, "Please enter a hexadecimal to convert to base64");

        try {
            var decodeHex = Hex.decodeHex(s);
            return Base64.encodeBase64String(decodeHex);
        } catch (DecoderException e) {
            return "Please pass a standard hexadecimal number";
        }
    }
}
