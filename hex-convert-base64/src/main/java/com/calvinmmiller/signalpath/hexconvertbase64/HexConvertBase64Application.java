package com.calvinmmiller.signalpath.hexconvertbase64;

import com.calvinmmiller.signalpath.hexconvertbase64.utility.Converter;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class HexConvertBase64Application implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(HexConvertBase64Application.class, args);
    }

    @Override
    public void run(String... args){
        System.out.println(args.length > 0 ? Converter.convertHexToBase64(args[0]) : "Please re-run and pass a hexadecimal argument!");
    }
}
