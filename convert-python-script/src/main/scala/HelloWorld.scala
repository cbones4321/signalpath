import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer

object HelloWorld {
  def main(args: Array[String]): Unit = {
    println(nonRecursive(args(0).toInt))
  }

  def nonRecursive(n: Int): BigInt = {
    if(n == 0 || n == 1 ){
      return n;
    }

    val arr = ListBuffer[BigInt](0, 1, 3)

    val c = 3
    for(x <- 0 to (n - 2)){
      arr.addOne((arr.last + arr(arr.size - 2)) + (c + x))
    }

    arr(n)
  }

  /*
  * An attempt to use tail recursion. Couldn't get it working :(
   */
  @tailrec
  def sumTo(n: Int, acc: Int = 0): Int = {
    if(n == 0 || n == 1) {
      acc + n
    } else {
      sumTo(n - 1, n - 2 + acc + n)
    }
  }
}
